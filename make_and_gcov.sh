#!/bin/bash

# Go into project directory
cd cpp_proj

# Make and run tests
echo $(tput setaf 3)Cleaning old files and making the project$(tput sgr0)
make clean
make
echo $(tput setaf 3)Running tests$(tput sgr0)
make test

# run gcov to generate gcov logfiles on all .cpp files
echo $(tput setaf 3)Generating gcov logfiles$(tput sgr0)
find ./ -type f -name *.cpp -exec gcov {} \;

echo $(tput setaf 3)Done$(tput sgr0)





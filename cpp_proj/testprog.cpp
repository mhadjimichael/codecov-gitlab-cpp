#include <stdio.h>

int main() {
    printf("Enter a number: ");
    
    int num;
    scanf("%d", &num);

    printf("Read: %d\n", num);

    if (num % 2 == 0) {
        printf("The number is even\n");
    } else {
        printf("The number is odd\n");
    }

    if (num < 0) {
        printf("The number is < 0\n");
    } else if (num == 0) {
        printf("The number is equal to 0\n");
    } else {
        printf("The number is > 0\n");
    }

    // This statement can never be true...
    if (num < 0 && num > 0) {
        printf("How is this even possible?!\n");
    }


    return 0;
}

